#!/usr/bin/env python3

# ----------------------
# collatz/TestCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ----------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cache_collatz

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    """
    test suit for collatz project
    """

    def __init__(self, *args, **kwargs):
        super(TestCollatz, self).__init__(*args, **kwargs)
        self.cache = cache_collatz()

    # ----
    # read
    # ----

    def test_read(self):
        """
        test for reading input
        """
        string_temp = "1 10\n"
        num_one, num_two = collatz_read(string_temp)
        self.assertEqual(num_one, 1)
        self.assertEqual(num_two, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        test 1
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(1, 10, self.cache)
        self.assertEqual(collatz_val, 20)

    def test_eval_2(self):
        """
        test 2
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(100, 200, self.cache)
        self.assertEqual(collatz_val, 125)

    def test_eval_3(self):
        """
        test 3
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(201, 210, self.cache)
        self.assertEqual(collatz_val, 89)

    def test_eval_4(self):
        """
        test 4
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(900, 1000, self.cache)
        self.assertEqual(collatz_val, 174)

    def test_eval_5(self):
        """
        test 5
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(1001, 2000, self.cache)
        self.assertEqual(collatz_val, 182)

    def test_eval_6(self):
        """
        test 6
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(2001, 3000, self.cache)
        self.assertEqual(collatz_val, 217)

    def test_eval_7(self):
        """
        test 7
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(3001, 4000, self.cache)
        self.assertEqual(collatz_val, 238)

    def test_eval_8(self):
        """
        test 8
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(4001, 5000, self.cache)
        self.assertEqual(collatz_val, 215)

    def test_eval_9(self):
        """
        test 9
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(5001, 6000, self.cache)
        self.assertEqual(collatz_val, 236)

    def test_eval_10(self):
        """
        test 10
        check if core algorithm is correct
        """
        collatz_val = collatz_eval(800001, 899999, self.cache)
        self.assertEqual(collatz_val, 525)

    def test_eval_11(self):
        """
        test 11
        check if core algorithm is correct
        Also, if the first input is bigger than the second one,
        collatz_eval should handle this case properly
        """
        collatz_val = collatz_eval(899999, 800001, self.cache)
        self.assertEqual(collatz_val, 525)

    # -----
    # print
    # -----

    def test_print(self):
        """
        test for print functionality"
        """
        standard_output = StringIO()
        collatz_print(standard_output, 1, 10, 20)
        self.assertEqual(standard_output.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        test to check if collatz_solve correctly output its result
        """
        standard_input = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        standard_output = StringIO()
        collatz_solve(standard_input, standard_output)
        self.assertEqual(standard_output.getvalue(),
                         "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
